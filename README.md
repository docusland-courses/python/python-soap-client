# Exemple d'utilisation d'un Web Service SOAP.

Utilisation du WSDL suivant :

- http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL
  Vous pouvez tester le WSDL ici :
- http://soapclient.com/soaptest.html

Votre objectif est d'utiliser ce web service afin de générer la liste suivante :

- Code du pays
- Nom du pays
- Capitale du pays
- Devise du pays

Je vous invite à utiliser la bibliothèque suivante :

```python
from zeep import Client
```
